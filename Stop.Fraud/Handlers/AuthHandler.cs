﻿using Stop.Fraud.Models;
using Stop.Fraud.Services;
using Stop.Fraud.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
namespace Stop.Fraud.Handlers
{
    public class AuthHandler : DelegatingHandler
    {
        private const string SCHEME = "Bearer";
        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            ErrorWriter.WriteLog("Request came in");
            ErrorWriter.WriteLog("//Headers");
            foreach (var item in request.Headers)
            {
                ErrorWriter.WriteLog($"{item.Key}: {String.Join(Environment.NewLine, item.Value)}");
            }
            ErrorWriter.WriteLog("//Body");
            ErrorWriter.WriteLog(await request.Content.ReadAsStringAsync());
            HttpResponseMessage response = null;
            try
            {
                if (request.Headers.Authorization != null && request.Headers.Authorization.Scheme.Equals(SCHEME))
                {
                    if (!String.IsNullOrEmpty(request.Headers.Authorization.Parameter))
                    {
                        var publicKey = request.Headers.Authorization.Parameter;
                        IReportingEntityService reportingEntityService = new ReportingEntityService();
                        var entity = reportingEntityService.GetByPublicKey(publicKey);
                        if (entity != null)
                        {
                            if (entity.VerificationStatus == VerificationStatus.Verified)
                            {
                                var claims = new List<Claim> { new Claim(ClaimTypes.Name, entity.PublicKey) };
                                var principal = new ClaimsPrincipal(new[] { new ClaimsIdentity(claims, SCHEME) });
                                Thread.CurrentPrincipal = principal;
                                if (HttpContext.Current != null)
                                {
                                    HttpContext.Current.User = principal;
                                }
                            }
                            else
                            {
                                response = request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Your account has not been verified, please upload your incorporation documents for verification or contact support@wallet.ng");
                                return response;
                            }
                        }
                        else
                        {
                            response = request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Invalid Public Key");
                            return response;
                        }
                    }
                    else
                    {
                        response = request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Public Key Not Provided");
                    }
                }
                else
                {
                    response = request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Authorization header is required");
                    response.Headers.WwwAuthenticate.Add(new System.Net.Http.Headers.AuthenticationHeaderValue(SCHEME));
                    return response;
                }
                response = await base.SendAsync(request, cancellationToken);
                return response;
            }
            catch (Exception ex)
            {
                ErrorWriter.WriteLog(ex.ToString());
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "An Exception occured while processing your request");
            }
        }
    }
}