﻿using Stop.Fraud.Services;
using Stop.Fraud.Services.CommunicationObjects;
using Stop.Fraud.Services.ResponseObjects;
using Stop.Fraud.Services.Utilities;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Hosting;
using System.Web.Http.ModelBinding;

namespace Stop.Fraud.Controllers
{
    [RoutePrefix("record")]
    public class RecordController : ApiController
    {
        public RecordController()
        {
            fraudService = new FraudService();
            reportingEntityService = new ReportingEntityService();
        }
        private IFraudService fraudService;
        private IReportingEntityService reportingEntityService;
        [Route("create")]
        [HttpPost]
        [ResponseType(typeof(GenericResponse))]
        public HttpResponseMessage CreateFraud(CreateFraudRequest request)
        {
            GenericResponse responseObj;
            HttpResponseMessage response;
            HttpRequestMessage requestObj = new HttpRequestMessage();
            requestObj.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            try
            {
                
                if (request != null && ModelState.IsValid)
                {
                    var entity = reportingEntityService.GetByPublicKey(User.Identity.Name);
                    if (entity != null)
                    {
                        if (entity.SecretKey == request.SecretKey)
                        {
                            string message = "";
                            if (fraudService.CreateFraud(request, entity.ReportingEntityId, out message))
                            {
                                responseObj = new GenericResponse
                                {
                                    Message = "Fraud record inserted successfully",
                                    ResponseCode = "200"
                                };
                                response = requestObj.CreateResponse(HttpStatusCode.OK, responseObj);
                            }
                            else
                            {
                                responseObj = new GenericResponse
                                {
                                    Message = message,
                                    ResponseCode = "400"
                                };
                                response = requestObj.CreateResponse(HttpStatusCode.BadRequest, responseObj);
                            }
                        }
                        else
                        {
                            responseObj = new GenericResponse
                            {
                                Message = "Secret key is not equal to public key",
                                ResponseCode = "500"
                            };
                            response = requestObj.CreateResponse(HttpStatusCode.BadRequest, responseObj);
                        }
                    }
                    else
                    {
                        responseObj = new GenericResponse
                        {
                            Message = "Can not find details for this entity, please check your public key",
                            ResponseCode = "500"
                        };
                        response = requestObj.CreateResponse(HttpStatusCode.BadRequest, responseObj);
                    }
                }
                else
                {
                    var modelError = "";
                    foreach (ModelState modelState in ModelState.Values)
                    {
                        foreach (var item in modelState.Errors)
                        {
                            modelError += item.ErrorMessage;
                        }
                    }
                    responseObj = new GenericResponse { ResponseCode = "400", Message = modelError };
                    response = requestObj.CreateResponse(HttpStatusCode.BadRequest, responseObj);
                }
            }
            catch (Exception ex)
            {
                ErrorWriter.WriteLog(ex.ToString());
                responseObj = new GenericResponse
                {
                    Message = "This service is currently not available, Please try again later",
                    ResponseCode = "500"
                };
                response = requestObj.CreateResponse(HttpStatusCode.InternalServerError, responseObj);
            }
            return response;
        }
        [Route("verify")]
        [HttpPost]
        [ResponseType(typeof(VerifyResponse))]
        public HttpResponseMessage Verify(VerifyFraudRequest request)
        {
            VerifyResponse responseObj;
            HttpResponseMessage response;
            HttpRequestMessage requestObj = new HttpRequestMessage();
            requestObj.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            try
            {

                if (request != null && ModelState.IsValid)
                {
                    var entity = reportingEntityService.GetByPublicKey(User.Identity.Name);
                    if (entity != null)
                    {
                        if (entity.SecretKey == request.SecretKey)
                        {
                            responseObj = fraudService.VerifyRecord(request.Record, entity.Name);
                            response = requestObj.CreateResponse(HttpStatusCode.OK, responseObj);
                        }
                        else
                        {
                            responseObj = new VerifyResponse
                            {
                                Message = "Secret key is not equal to public key",
                                ResponseCode = "500"
                            };
                            response = requestObj.CreateResponse(HttpStatusCode.BadRequest, responseObj);
                        }
                    }
                    else
                    {
                        responseObj = new VerifyResponse
                        {
                            Message = "Can not find details for this entity, please check your public key",
                            ResponseCode = "500"
                        };
                        response = requestObj.CreateResponse(HttpStatusCode.BadRequest, responseObj);
                    }
                }
                else
                {
                    var modelError = "";
                    foreach (ModelState modelState in ModelState.Values)
                    {
                        foreach (var item in modelState.Errors)
                        {
                            modelError += item.ErrorMessage;
                        }
                    }
                    responseObj = new VerifyResponse { ResponseCode = "400", Message = modelError };
                    response = requestObj.CreateResponse(HttpStatusCode.BadRequest, responseObj);
                }
            }
            catch (Exception ex)
            {
                ErrorWriter.WriteLog(ex.ToString());
                responseObj = new VerifyResponse
                {
                    Message = "This service is currently not available, Please try again later",
                    ResponseCode = "500"
                };
                response = requestObj.CreateResponse(HttpStatusCode.InternalServerError, responseObj);
            }
            return response;
        }
    }
}
