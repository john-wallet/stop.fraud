﻿using Stop.Fraud.Models;
using Stop.Fraud.Services.CommunicationObjects;
using Stop.Fraud.Services.ResponseObjects;
using Stop.Fraud.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Stop.Fraud.Services
{
    public class FraudService : IFraudService
    {
        public FraudService()
        {
            unitOfWork = new UnitOfWork();
        }
        private UnitOfWork unitOfWork;
        public bool CreateFraud(CreateFraudRequest request, long reportingEntity, out string message)
        {
            var hashedRecord = HashUtil.Hash(request.Record);
            //if (FraudRecordExists(hashedRecord))
            //{
            //    message = "This record already exists";
            //    return false;
            //}
            //else
            //{
                Models.Fraud fraud = new Models.Fraud
                {
                    DateCreated = DateTime.Now,
                    DateOfFraud = Convert.ToDateTime(request.FraudDate),
                    FraudRecord = hashedRecord,
                    Description = request.Description,
                    ExtendedDescription = request.ExtendedDescription,
                    FraudType = GetFraudType(request.FraudType),
                    RecordType = GetRecordType(request.RecordType),
                    ReportingEntityId = reportingEntity,
                    ReportingStatus = ReportingStatus.Active,
                    ReportingEntityInternalReference = request.ReportingReference,
                    DateDeleted = DateTime.Now,
                    DateLastModified = DateTime.Now,
                };
                unitOfWork.FraudRepository.Insert(fraud);
                Activity activity = new Activity
                {
                    ActivityType = ActivityType.Report,
                    CreatedBy = reportingEntity.ToString(),
                    CreatedOn = DateTime.Now,
                    DateDeleted = DateTime.Now,
                    FraudRecordId = fraud.FraudId,
                };
                unitOfWork.ActivityRepository.Insert(activity);
                unitOfWork.Save();
                message = "Fraud created successfully";
                return true;
            //}
        }
        public VerifyResponse VerifyRecord(string fraudRecord, string reportingEntity)
        {
            var hashedRecord = HashUtil.Hash(fraudRecord);
            var fraudRecords = unitOfWork.FraudRepository.Get(c => c.FraudRecord == hashedRecord, includeProperties: "ReportingEntity");
            Activity activity = new Activity
            {
                ActivityType = ActivityType.View,
                CreatedBy = reportingEntity.ToString(),
                CreatedOn = DateTime.Now,
                DateDeleted = DateTime.Now
            };
            unitOfWork.ActivityRepository.Insert(activity);
            unitOfWork.Save();
            if (fraudRecords.Count() < 1)
            {
                return new VerifyResponse
                {
                    ResponseCode = "400",
                    Message = "Record does not exist"
                };
            }
            else
            {
                return new VerifyResponse
                {
                    Count = fraudRecords.Count(),
                    DateOfFirstFraud = fraudRecords.FirstOrDefault().DateOfFraud.ToString(),
                    DateOfLastFraud = fraudRecords.LastOrDefault().DateOfFraud.ToString(),
                    ResponseCode = "200",
                    Message = "Record exists in the database",
                    Data = fraudRecords.Select(x => new VerifyResponseData
                    {
                        DateCreated = x.DateCreated.ToString(),
                        DateofFraud = x.DateOfFraud.ToString(),
                        Description = x.Description,
                        ExtendedDescription = x.ExtendedDescription,
                        ReportingCompany = x.ReportingEntity.Name
                    }).ToList()
                };
            }
        }
        public bool FraudRecordExists(string record)
        {
            return unitOfWork.FraudRepository.Any(c => c.FraudRecord == record);
        }
        public FraudType GetFraudType(int fraudType)
        {
            switch (fraudType)
            {
                case 1:
                    return FraudType.Impersonation;
                case 2:
                    return FraudType.AccountHijack;
                case 3:
                    return FraudType.Phishing;
                default:
                    return FraudType.Other;
            }
        }
        public RecordType GetRecordType(int recordType)
        {
            switch (recordType)
            {
                case 1:
                    return RecordType.Email;
                case 2:
                    return RecordType.IPAddress;
                case 3:
                    return RecordType.AccountNumber;
                case 4:
                    return RecordType.CardPAN;
                case 5:
                    return RecordType.PhoneNumber;
                default:
                    return RecordType.Other;
            }
        }
    }
}
