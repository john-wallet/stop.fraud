﻿using Stop.Fraud.Services.CommunicationObjects;
using Stop.Fraud.Services.ResponseObjects;
using System.Collections.Generic;

namespace Stop.Fraud.Services
{
    public interface IFraudService
    {
        bool CreateFraud(CreateFraudRequest request, long reportingEntity, out string message);
        VerifyResponse VerifyRecord(string fraudRecord, string reportingEntity);
    }
}
