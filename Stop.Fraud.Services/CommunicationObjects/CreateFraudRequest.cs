﻿using System.ComponentModel.DataAnnotations;

namespace Stop.Fraud.Services.CommunicationObjects
{
    public class CreateFraudRequest
    {
        [Required]
        [MaxLength(100)]
        public string Record { get; set; }
        public int RecordType { get; set; }
        public int FraudType { get; set; }
        [Required]
        [MaxLength(1000)]
        public string Description { get; set; }
        [MaxLength(10000)]
        public string ExtendedDescription { get; set; }
        [MaxLength(100)]
        public string ReportingReference { get; set; }
        [Required]
        public string SecretKey { get; set; }
        public string FraudDate { get; set; }
    }
}
