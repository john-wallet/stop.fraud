﻿using System;
using System.ComponentModel.DataAnnotations;
namespace Stop.Fraud.Services.CommunicationObjects
{
    public class VerifyFraudRequest
    {
        [Required]
        public string Record { get; set; }
        [Required]
        public string SecretKey { get; set; }
    }
}
