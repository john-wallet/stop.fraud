﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stop.Fraud.Services.CommunicationObjects
{
    public class CreateEntityRequest
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        public int BusinessType { get; set; }
        [Required]
        [MaxLength(500)]
        public string Address { get; set; }
        [Required]
        [MaxLength(50)]
        public string City { get; set; }
        [Required]
        [MaxLength(50)]
        public string Country { get; set; }
        [Required]
        [MaxLength(200)]
        public string ContactName { get; set; }
        [Required]
        [MaxLength(200)]
        public string ContactEmail { get; set; }
        [Required]
        [MaxLength(50)]
        public string ContactPhone { get; set; }
        [Required]
        [MaxLength(20)]
        public string ContactRole { get; set; }
    }
}
