﻿using Stop.Fraud.Models;
using Stop.Fraud.Services.CommunicationObjects;
using Stop.Fraud.Services.Utilities;
using System;
using System.Linq;
namespace Stop.Fraud.Services
{
    public class ReportingEntityService : IReportingEntityService
    {
        public ReportingEntityService()
        {
            unitOfWork = new UnitOfWork();
        }
        private UnitOfWork unitOfWork;
        public ReportingEntity GetByPublicKey(string publicKey)
        {
            return unitOfWork.ReportingEntityRepository.Get(c => c.PublicKey == publicKey).FirstOrDefault();
        }
        public bool Create(CreateEntityRequest request, out string message)
        {
            if (!ContactEmailExists(request.ContactEmail))
            {
                ReportingEntity reportingEntity = new ReportingEntity
                {
                    Address = request.Address,
                    City = request.City,
                    BusinessType = GetBusinessType(request.BusinessType),
                    ContactEmail = request.ContactEmail,
                    ContactName = request.ContactName,
                    Name = request.Name,
                    ContactPhone = request.ContactPhone,
                    ContactRole = request.ContactRole,
                    Country = request.Country,
                    PublicKey = GeneratePublicKey(),
                    SecretKey = GenerateSecretKey(),
                    CreatedOn = DateTime.Now,
                    DateDeleted = DateTime.Now,
                    DateLastModified = DateTime.Now,
                    CreatedBy = "Admin",
                    VerificationStatus = VerificationStatus.Unverified,
                };
                unitOfWork.ReportingEntityRepository.Insert(reportingEntity);
                unitOfWork.Save();
                message = "Entity created successfully";
                return true;
            }
            else
            {
                message = "Email already exists";
                return false;
            }
            
        }
        public string GenerateSecretKey()
        {
            string code = $"sk_{CodeGenerator.GenerateAlphaNumeric(10)}";
            while (PublicKeyExists(code))
            {
                code = $"sk_{CodeGenerator.GenerateAlphaNumeric(10)}";
            }
            return code;
        }
        public string GeneratePublicKey()
        {
            string code = $"pk_{CodeGenerator.GenerateAlphaNumeric(10)}";
            while (PublicKeyExists(code))
            {
                code = $"pk_{CodeGenerator.GenerateAlphaNumeric(10)}";
            }
            return code;
        }
        public bool PublicKeyExists(string code)
        {
            return unitOfWork.ReportingEntityRepository.Any(filter: v => v.PublicKey == code);
        }
        public bool ContactEmailExists(string email)
        {
            return unitOfWork.ReportingEntityRepository.Any(filter: v => v.ContactEmail == email);
        }
        public bool SecretKeyExists(string code)
        {
            return unitOfWork.ReportingEntityRepository.Any(filter: v => v.SecretKey == code);
        }
        public BusinessType GetBusinessType(int recordType)
        {
            switch (recordType)
            {
                case 1:
                    return BusinessType.Bank;
                case 2:
                    return BusinessType.PaymentServiceProvider;
                case 3:
                    return BusinessType.Switch;
                case 4:
                    return BusinessType.LawEnforcement;
                case 5:
                    return BusinessType.ECommerce;
                default:
                    return BusinessType.Other;
            }
        }
    }
}
