﻿namespace Stop.Fraud.Services.ResponseObjects
{
    public class GenericResponse
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
    }
}
