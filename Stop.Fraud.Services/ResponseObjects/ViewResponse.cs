﻿using System.Collections.Generic;

namespace Stop.Fraud.Services.ResponseObjects
{
    public class VerifyResponse : GenericResponse
    {
        public List<VerifyResponseData> Data { get; set; }
        public int Count { get; set; }
        public string DateOfFirstFraud { get; set; }
        public string DateOfLastFraud { get; set; }
    }
    public class VerifyResponseData
    {
        public string Description { get; set; }
        public string ExtendedDescription { get; set; }
        public string ReportingCompany { get; set; }
        public string DateCreated { get; set; }
        public string DateofFraud { get; set; }
    }
}
