﻿using Stop.Fraud.Models;
using Stop.Fraud.Services.CommunicationObjects;

namespace Stop.Fraud.Services
{
    public interface IReportingEntityService
    {
        bool Create(CreateEntityRequest request, out string message);
        ReportingEntity GetByPublicKey(string publicKey);
    }
}
