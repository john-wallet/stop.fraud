﻿using System;
using System.Linq;
namespace Stop.Fraud.Services.Utilities
{
    public class CodeGenerator
    {
        public static Random random = new Random();
        public static string Generate(int size)
        {
            string input = "0123456789";
            var chars = Enumerable.Range(0, size)
                                   .Select(x => input[random.Next(0, input.Length)]);
            return new string(chars.ToArray());
        }
        public static string GenerateAlphaNumeric(int size)
        {
            string input = "abcdefghijklmnopqrstuvwxyz0123456789";
            var chars = Enumerable.Range(0, size)
                                   .Select(x => input[random.Next(0, input.Length)]);
            return new string(chars.ToArray());
        }
        public static string GenerateAlphabet(int size)
        {
            string input = "ABCDEFGHJKLMNPQRSTUVWXYZ";
            var chars = Enumerable.Range(0, size)
                                   .Select(x => input[random.Next(0, input.Length)]);
            return new string(chars.ToArray());
        }
        public static string GenerateAlphaNumericReference()
        {
            string firstPart = GenerateAlphabet(1);
            string secondPart = Generate(6);
            return firstPart + secondPart;
        }
        public static string GenerateAlphaNumericReference(int alphabetLength, int numericLength)
        {
            string firstPart = GenerateAlphabet(alphabetLength);
            string secondPart = Generate(numericLength);
            return firstPart + secondPart;
        }
    }
}
