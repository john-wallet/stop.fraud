﻿using System;
using System.Web;
using System.IO;
namespace Stop.Fraud.Services.Utilities
{
    public class ErrorWriter
    {
        public static void WriteLog(string msg)
        {
            HttpContext context = HttpContext.Current;
            string path = Path.Combine(HttpRuntime.AppDomainAppPath, "App_Logs/ErrorLog.txt");
            StreamWriter writer = new StreamWriter(path, true);
            writer.WriteLine(msg);
            writer.WriteLine(DateTime.Now);
            writer.Close();
        }
    }
}