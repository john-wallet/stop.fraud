﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;

namespace Stop.Fraud.Services.Utilities
{
    public class HashUtil
    {
        public static string Hash(string id)
        {
            string key = "";
            System.Security.Cryptography.SHA512 hashtool = new System.Security.Cryptography.SHA512Managed();
            Byte[] strHash = System.Text.Encoding.UTF8.GetBytes(string.Concat(id));
            Byte[] encryptedHash = hashtool.ComputeHash(strHash);
            hashtool.Clear();
            StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < encryptedHash.Length; i++)
            {
                hexString.Append(String.Format("{0:X2}", encryptedHash[i]));
            }
            key = hexString.ToString();
            return key;
        }
        public static string TripleDESEncrypt(string toEncrypt, string key, bool useHashing = true, bool useSafeUrl = false)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
            
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));

                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;

            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray =
              cTransform.TransformFinalBlock(toEncryptArray, 0,
              toEncryptArray.Length);
            tdes.Clear();
            if (useSafeUrl)
            {
                return ToUrlSafeBase64(resultArray);
            }
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string TripleDESDecrypt(string cipherString, string key, bool useHashing = true, bool useSafeUrl = false)
        {
            byte[] keyArray, toEncryptArray;
            if (useSafeUrl)
            {
                toEncryptArray = FromUrlSafeBase64(cipherString);
            }
            else toEncryptArray = Convert.FromBase64String(cipherString);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));

                hashmd5.Clear();
            }
            else
            {
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;

            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
        public static string ToUrlSafeBase64(byte[] bytes)
        {
            return Convert.ToBase64String(bytes).Replace('+', '-').Replace('/', '_').Replace("=", "");
        }

        public static byte[] FromUrlSafeBase64(string s)
        {
            while (s.Length % 4 != 0)
                s += "=";
            s = s.Replace('-', '+').Replace('_', '/');
            return Convert.FromBase64String(s);
        }
    }
}