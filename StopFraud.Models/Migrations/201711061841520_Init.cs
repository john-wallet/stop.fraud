namespace StopFraud.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        ActivityId = c.Long(nullable: false, identity: true),
                        FraudRecordId = c.Long(nullable: false),
                        ActivityType = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        Deleted = c.Boolean(nullable: false),
                        DateDeleted = c.DateTime(nullable: false),
                        DeletedBy = c.String(maxLength: 1000),
                    })
                .PrimaryKey(t => t.ActivityId);
            
            CreateTable(
                "dbo.Frauds",
                c => new
                    {
                        FraudId = c.Long(nullable: false, identity: true),
                        FraudType = c.Int(nullable: false),
                        RecordType = c.Int(nullable: false),
                        FraudRecord = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false, maxLength: 1000),
                        ExtendedDescription = c.String(),
                        ReportingEntityId = c.Long(nullable: false),
                        ReportingEntityInternalReference = c.String(maxLength: 100),
                        ReportingStatus = c.Int(nullable: false),
                        DateOfFraud = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateLastModified = c.DateTime(nullable: false),
                        ModifiedBy = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DateDeleted = c.DateTime(nullable: false),
                        DeletedBy = c.String(maxLength: 1000),
                    })
                .PrimaryKey(t => t.FraudId)
                .ForeignKey("dbo.ReportingEntities", t => t.ReportingEntityId, cascadeDelete: true)
                .Index(t => t.ReportingEntityId);
            
            CreateTable(
                "dbo.ReportingEntities",
                c => new
                    {
                        ReportingEntityId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        BusinessType = c.Int(nullable: false),
                        Address = c.String(nullable: false, maxLength: 500),
                        City = c.String(nullable: false, maxLength: 50),
                        Country = c.String(nullable: false, maxLength: 50),
                        ContactName = c.String(nullable: false, maxLength: 200),
                        ContactEmail = c.String(nullable: false, maxLength: 200),
                        ContactPhone = c.String(nullable: false, maxLength: 50),
                        ContactRole = c.String(nullable: false, maxLength: 20),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.String(nullable: false, maxLength: 100),
                        DateLastModified = c.DateTime(nullable: false),
                        ModifiedBy = c.DateTime(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DateDeleted = c.DateTime(nullable: false),
                        DeletedBy = c.String(maxLength: 1000),
                        SecretKey = c.String(nullable: false, maxLength: 20),
                        PublicKey = c.String(nullable: false, maxLength: 20),
                        VerificationStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ReportingEntityId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Frauds", "ReportingEntityId", "dbo.ReportingEntities");
            DropIndex("dbo.Frauds", new[] { "ReportingEntityId" });
            DropTable("dbo.ReportingEntities");
            DropTable("dbo.Frauds");
            DropTable("dbo.Activities");
        }
    }
}
