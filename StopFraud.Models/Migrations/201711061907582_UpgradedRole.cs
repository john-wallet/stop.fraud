namespace StopFraud.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpgradedRole : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ReportingEntities", "ModifiedBy", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ReportingEntities", "ModifiedBy", c => c.DateTime(nullable: false));
        }
    }
}
