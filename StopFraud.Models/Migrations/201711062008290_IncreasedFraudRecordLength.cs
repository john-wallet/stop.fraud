namespace StopFraud.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncreasedFraudRecordLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Frauds", "FraudRecord", c => c.String(nullable: false, maxLength: 1000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Frauds", "FraudRecord", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
