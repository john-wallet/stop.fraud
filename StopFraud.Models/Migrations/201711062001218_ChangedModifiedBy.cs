namespace StopFraud.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedModifiedBy : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Frauds", "ModifiedBy", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Frauds", "ModifiedBy", c => c.DateTime(nullable: false));
        }
    }
}
