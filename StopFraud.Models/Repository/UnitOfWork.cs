﻿using System;
namespace Stop.Fraud.Models
{
    public class UnitOfWork : IDisposable
    {
        private DatabaseContext context = new DatabaseContext();
        private GenericRepository<Activity> activityRepository;
        private GenericRepository<Fraud> fraudRepository;
        private GenericRepository<ReportingEntity> reportingEntityRepository;
        public GenericRepository<Activity> ActivityRepository
        {
            get
            {
                if (this.activityRepository == null)
                {
                    this.activityRepository = new GenericRepository<Activity>(context);
                }
                return activityRepository;
            }
        }
        public GenericRepository<Fraud> FraudRepository
        {
            get
            {
                if (this.fraudRepository == null)
                {
                    this.fraudRepository = new GenericRepository<Fraud>(context);
                }
                return fraudRepository;
            }
        }
        public GenericRepository<ReportingEntity> ReportingEntityRepository
        {
            get
            {
                if (this.reportingEntityRepository == null)
                {
                    this.reportingEntityRepository = new GenericRepository<ReportingEntity>(context);
                }
                return reportingEntityRepository;
            }
        }
        public void Save()
        {
            context.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}