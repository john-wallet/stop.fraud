﻿using System.Data.Entity;

namespace Stop.Fraud.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("name=AppCon")
        {

        }
        public virtual DbSet<Activity> Activities { get; set; }
        public virtual DbSet<Fraud> Frauds { get; set; }
        public virtual DbSet<ReportingEntity> ReportingEntities { get; set; }
    }
}
