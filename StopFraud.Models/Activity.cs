﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Stop.Fraud.Models
{
    public class Activity
    {
        public long ActivityId { get; set; }
        public long FraudRecordId { get; set; }
        public ActivityType ActivityType { get; set; }
        public DateTime CreatedOn { get; set; }
        [MaxLength(100)]
        public string CreatedBy { get; set; }
        public bool Deleted { get; set; }
        public DateTime DateDeleted { get; set; }
        [MaxLength(1000)]
        public string DeletedBy { get; set; }
    }
    public enum ActivityType
    {
        View = 1,
        Report = 2
    }
}
