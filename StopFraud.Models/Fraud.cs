﻿using System.ComponentModel.DataAnnotations;
using System;
namespace Stop.Fraud.Models
{
    public class Fraud
    {
        public long FraudId { get; set; }
        public FraudType FraudType { get; set; }
        public RecordType RecordType { get; set; }
        [Required]
        [MaxLength(1000)]
        public string FraudRecord { get; set; }
        [Required]
        [MaxLength(1000)]
        public string Description { get; set; }
        [MaxLength(10000)]
        public string ExtendedDescription { get; set; }
        public long ReportingEntityId { get; set; }
        [MaxLength(100)]
        public string ReportingEntityInternalReference { get; set; }
        public ReportingStatus ReportingStatus { get; set; }
        public DateTime DateOfFraud { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public string ModifiedBy { get; set; }
        public bool Deleted { get; set; }
        public DateTime DateDeleted { get; set; }
        [MaxLength(1000)]
        public string DeletedBy { get; set; }
        public virtual ReportingEntity ReportingEntity { get; set; }
    }
    public enum FraudType
    {
        Impersonation = 1,
        AccountHijack = 2,
        Phishing = 3,
        Other = 4
    }
    public enum RecordType
    {
        Email = 1,
        IPAddress = 2,
        AccountNumber = 3,
        CardPAN = 4,
        PhoneNumber = 5,
        Other = 6
    }
    public enum ReportingStatus
    {
        Active = 1,
        Inactive = 2,
        Resolved = 3
    }
}
