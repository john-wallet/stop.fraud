﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Stop.Fraud.Models
{
    public class ReportingEntity
    {
        public long ReportingEntityId { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        public BusinessType BusinessType { get; set; }
        [Required]
        [MaxLength(500)]
        public string Address { get; set; }
        [Required]
        [MaxLength(50)]
        public string City { get; set; }
        [Required]
        [MaxLength(50)]
        public string Country { get; set; }
        [Required]
        [MaxLength(200)]
        public string ContactName { get; set; }
        [Required]
        [MaxLength(200)]
        public string ContactEmail { get; set; }
        [Required]
        [MaxLength(50)]
        public string ContactPhone { get; set; }
        [Required]
        [MaxLength(20)]
        public string ContactRole { get; set; }
        public DateTime CreatedOn { get; set; }
        [Required]
        [MaxLength(100)]
        public string CreatedBy { get; set; }
        public DateTime DateLastModified { get; set; }
        public string ModifiedBy { get; set; }
        public bool Deleted { get; set; }
        public DateTime DateDeleted { get; set; }
        [MaxLength(1000)]
        public string DeletedBy { get; set; }
        [Required]
        [MaxLength(20)]
        public string SecretKey { get; set; }
        [Required]
        [MaxLength(20)]
        public string PublicKey { get; set; }
        public ICollection<Fraud> Frauds { get; set; }
        public VerificationStatus VerificationStatus { get; set; }
    }
    public enum BusinessType
    {
        Bank = 1,
        PaymentServiceProvider = 2,
        Switch = 3,
        LawEnforcement = 4,
        ECommerce = 5,
        Other = 6
    }
    public enum VerificationStatus
    {
        Unverified = 1,
        Verified = 2
    }
}
