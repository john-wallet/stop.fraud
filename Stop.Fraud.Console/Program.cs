﻿using Stop.Fraud.Services;
using Stop.Fraud.Services.CommunicationObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stop.Fraud.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            IReportingEntityService reportingEntityService = new ReportingEntityService();
            var entityRequest = new CreateEntityRequest
            {
                Address = "6, Samuel Omidiji Street, Badore, Ajah",
                BusinessType = 1,
                City = "Lagos",
                ContactEmail = "hello@wallet.ng",
                ContactName = "John Oke",
                ContactPhone = "07068264085",
                ContactRole = "C.E.O",
                Country = "Nigeria",
                Name = "Wallet.ng"
            };
            var message = "";
            reportingEntityService.Create(entityRequest, out message);
        }
    }
}
